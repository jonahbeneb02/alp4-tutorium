---
theme: "metropolis"
aspectratio: 169
header-includes: |
        \usepackage{amsmath}
        \usepackage{qrcode}
        \usepackage{hyperref}
---

# Tutorium 08

## Lehrevaluation

\begin{center}
\qrcode[height=5cm]{https://lehrevaluation.fu-berlin.de/productive/de/sl/r1chLrudVfeG}
\end{center}

\begin{center}
\href{https://lehrevaluation.fu-berlin.de/productive/de/sl/r1chLrudVfeG}{lehrevaluation.fu-berlin.de/productive/de/sl/r1chLrudVfeG}
\end{center}

<!-- \qrcode{https://lehrevaluation.fu-berlin.de/productive/de/sl/r1chLrudVfeG} -->

## Hausaufgabe

- Übung 07 vorstellen
- Übung 08 besprechen

## Multicast

Wie unterscheiden sich Unicast und Multicast?

## REST

- Resourcen werden über URLs identifiziert
- HTTP Methoden für Resourcen:
    - `PUT`: Neue Resource erstellen (idempotent)
    - `GET`: Zustand der Resource abrufen
    - `DELETE`: Resource löschen
    - `POST`: Resource verändern / Resource erstellen


## JSON

Eingebautes Format von JavaScript für die Übertragung von Objekten

```json
{
    "property1": "Hello",
    "property2": 4,
    "subobj": {
    }
}
```

Kann als Body von `POST` und `PUT` requests versendet werden, und als Antwort von `GET` requests erhalten werden.

## REST

Überlegt euch die REST-Endpunkte für einen Service über den
die verfügbaren Kurse einer Universität verwaltet werden können.

## RPC

- Funktionen von entfernten Prozessen können über das Netzwerk aufgerufen werden.
- Funktionsargumente müssen für die Übertragung in ein einheitliches Format übersetzt werden
- Die kommunizierenden Prozesse können dadurch in unterschiedlichen Programmiersprachen geschrieben werden
 
## Übung

Welche Unterschiede ergeben sich beim Campus-Management-Beispiel wenn man statt REST RPC verwenden würde? 

## Übung

Was passiert wenn eine Dozentin die Anzahl der Plätze ändert, während jemand anderes gerade die Beschreibung des selben Moduls ändert?

## Übung

Wann macht es Sinn REST, und wann RPC zu verwenden?

## Call Transparency

Transparent: Ein Aufruf einer entfernten Funktion unterscheidet sich syntaktisch nicht von einer lokalen Funktion

Non-Transparent: Es ist klar erkennbar, dass es sich um den Aufruf einer Funktion eines entfernten Prozesses handelt

Welche Vor- und Nachteile gibt es?

## Skalierung

- Vertikal: Verschiedene Rechner erfüllen unterschiedliche Aufgaben, aber es gibt für jede Aufgabe nur einen.

- Horizontal: Viele Rechner erfüllen die selbe Aufgabe, und es kann unbegrenz viele einer Art geben.

## Übung

In welchen Situationen macht es Sinn auf vertikale, und in welchen auf hochizontale Skalierung zu optimieren?


## Bankers Algorithm

\begin{align*}
  \vec{v} = (6, 8, 3) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  4 & 4 & 1 \\
  1 & 1 & 2 \\
  0 & 3 & 0
  \end{pmatrix}
\end{align*}
\begin{align*}
  A = 
  \begin{pmatrix} 1 & 0 & 0 \\
  2 & 0 & 1 \\
  1 & 3 & 2
  \end{pmatrix}
\end{align*}

- Wie viele freie Resourcen ($f$) existieren?
- Können die angefragten Resourcen ($A$) von einem der Threads geliefert werden?
- Können alle Threads beendet werden?

