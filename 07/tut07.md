---
theme: "metropolis"
aspectratio: 169
header-includes: |
        \usepackage{amsmath}
        \usepackage{qrcode}
---
# Tutorium 07

## Hausaufgabe

- 06 Vorstellen
- 07 Besprechen

## Verteilte Systeme

<!--  Platzdeckchen Methode -->

- Was ist ein verteiltes System?

## Verteilte Systeme

- Was sind typische Eigenschaften von verteilten Systemen?

## Verteilte Systeme

- Was ist der Unterschied zwischen einem Server und einem Client?
- Welche Netzwerkprotokolle auf der Anwendungsschicht kennt ihr?

## Verteilte Systeme

- Was sind Vor- und Nachteile von dezentralen Systemen gegenüber zentralisierten Systemen? 

## Verteilte Systeme

- Findet Beispiele für Client-Server und Peer-to-Peer Architekturen

## Sockets

- Was waren TCP und UDP?

## Abstraktion

Welche Abstraktionsmöglichkeiten gibt es für die Programmierung von verteilten Systemen?

## Echo Server

- Programmiert in Python ein Programm, das ein TCP-Socket öffnet und alle empfangenen Nachrichten zurücksendet
- Das Programm kann zum Beispiel mit dem `nc`-Befehl getestet werden (`nc localhost 5000`)

