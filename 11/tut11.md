---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 11

## Übungszettel

- 10 vorstellen
- 11 besprechen


## Determinismus

Was ist der Unterschied zwischen Determinismus und Determiniertheit?

## Schutz des kritischen Abschnitts

Was sind die Anforderungen zum Schutz des kritischen Abschnitts?

<!--  
• The solution has to protect the critical section reliably by mutual exclusion.
• The solution should be used in higher level programming languages.
using it.
• The solution must not lead to a deadlock.
• The solution should provide low overhead.
• The access to the critical section should be fair.
-->

## Schutz des kritischen Abschnitts

Erfüllt dieses Lock alle Anforderungen zum Schutz des Kritischen Abschnitts?

```C
#define NUM_THREADS 2
char _lock[2];

int lock (long tid) {
    _lock[tid] = 1;
    while (_lock[NUM_THREADS - 1 - tid]) {}
    return 0;
}
int unlock (long tid) {
    _lock[tid] = 0;
    return 0;
}
````

## Schutz des kritischen Abschnitts

Twofold Lock with Mutual Access nach Peterson
```C
char _lock[2]; // init with 0 at main()
int favoured = 1;
int lock (long tid) {
    _lock[tid] = 1;
    favoured = NUM_THREADS - 1 - tid;
    while (_lock[NUM_THREADS-1-tid] && favoured == < hier code Einsetzen >) {}
    return 0;
}
int unlock (long tid) {
    favoured = NUM_THREADS - 1 - tid;
    _lock[tid] = 0;
    return 0;
}
```

## Schutz des kritischen Abschnitts

1.  `NUM_THREADS-1-tid`
2.  `lock[tid]`
3.  `favoured + tid - 1`

## Korrektheit

Welche Komponenten wirken sich auf die Korrektheit eines Programmes aus?

## Petri-Netze

Zeichne ein Petri-Netz zur Sicherung einer eingleisigen Bahnstrecke

## Petri-Netze

Was ist der Unterschied zwischen einem farbigen und einem nicht-farbigen Petri-Netz?

## Deadlocks

Welche Bedingungen müssen gegeben sein, damit ein Deadlock entstehen kann?

<!-- 
1. Resources are used exclusively.
2. Threads or processes hold allocation of a resource and try to allocate
another.
3. There is no preemption.
4. There is a cycle in the wait-for graph
-->

## Bankers Algorithm

\begin{align*}
  \vec{f} = (2, 3, 1) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  2 & 1 & 1 \\
  1 & 1 & 2 \\
  0 & 2 & 0
  \end{pmatrix}
\end{align*}
\begin{align*}
  A = 
  \begin{pmatrix}
  2 & 2 & 2 \\
  1 & 1 & 0 \\
  2 & 0 & 1
  \end{pmatrix}
\end{align*}

- Wie viele Resourcen ($v$) existieren?
- Können die angefragten Resourcen ($A$) von einem der Threads geliefert werden?
- Können alle Threads beendet werden?

## Semaphoren

```C
sem_t to_produce; // amount of data to produce
sem_t to_consume; // amount of data to consume
pthread_mutex_t mutex; // critical section
```

## Semaphoren

```C

void* Producer (void *threadid)
{
    for (int i= 0; i < 1000; i++) {
        
        buffer[last] = i;
        printf("Producer %d puts %d into
            buffer at place %d \n",
            (long) threadid, buffer[last],
            last);
        last++;
        
    }
    pthread_exit (NULL);
}
```

## OpenMP

Wie kann dieser code parallelisiert werden?
```C
#pragma omp parallel
{
    int out[10];
    int num_processed = 0;

    for (int i = 0; i < 10; i++) {
        out[i] = abs(i)
        
        num_processed += 1;
    }
}
```

## Foster's Design Methodology

Was sind die Schritte in diesem Vorgehen?

## Theorie

Was ist die maximale Geschwindigkeitssteigerung durch die Parallelisierung eines Programms?

## MPI

Was ist der Unterschied zwischen MPI_Send und MPI_Isend?

## Deadlocks

Kann es zu einem Deadlock kommen wenn man aus sicht des Betriebssystems einen (einzelnen) Drucker als Resource betrachtet?

## REST

Ordne die HTTP-Methoden den Anwendungsfällen zu:

* Alle Termine im Kalender abrufen
* Einen Eintrag löschen
* Einen Eintrag hinzufügen
* Die Uhrzeit eines Eintrags ändern

## REST

- Welche Unterschiede gibt es zwischen REST und RPC?

## Zeit

- Kann man aus der realen Zeit eines Ereignisses ableiten, ob seine Vektorzeit größer oder kleiner als die eines anderen Ereignisses ist?

## Map Reduce

Welche Schritte gibt es im Map-Reduce-Verfahren?

<!-- 
1. Input Splitting
2. Mapping
3. Shuffling and Sorting
4. Reducing
5. Output
-->

## Web

Was ist ein DOM?

## HTML

Wie sieht diese Webseite aus?
```HTML
<!DOCTYPE html>
<html>
    <body>
        <h1>Wichtige Ereignisse</h1>
        <ul>
            <li style="color: red">Stromausfall</li>
            <li>Europawahl</li>
            <li>Klausur</li>
        </ul>
    </body>
</html>
```
