---
theme: "metropolis"
aspectratio: 169
header-includes: \usepackage{amsmath}
---
# Tutorium 05

## Wiederholung

Ohne in die Vorlesung zu gucken überlegen:

- Was sind die vier Schritte von Fosters Design Methode?

<!--
1. Partitioning
2. Communication
3. Agglomeration
4. Mapping (Assignment)
-->

## Bankers Algorithm

\begin{align*}
  \vec{v} = (3, 5, 2) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix} 2 & 0 & 0 \\
  1 & 1 & 2 \\
  0 & 3 & 0
  \end{pmatrix}
\end{align*}
\begin{align*}
  A = 
  \begin{pmatrix} 1 & 1 & 0 \\
  2 & 0 & 1 \\
  1 & 2 & 0
  \end{pmatrix}
\end{align*}

- Wie viele freie Resourcen ($f$) existieren?
- Können die angefragten Resourcen ($A$) von einem der Threads geliefert werden?
- Können alle Threads beendet werden?

## Hausaufgabe

- Vorstellen
- nächste Besprechen

## Expertengruppen zu Foster's Design Methodology

- Partitioning
- Communication
- Agglomeration
- Mapping

## Gruppenarbeit

- Beispiel: Kochen
- Wie könnte man Fosters Design Methode anwenden?

## OpenMP

- Erweiterung für verschiedene Programmiersprachen, um Syntax für parallele
  Programmierung hinzuzufügen

## Wiederholung

```C
#pragma omp parallel shared (<variables>) private (<variables>)
```
Definiert welche Variablen nur auf einem Thread verwendet werden, und welche
zwischen Threads geteilt werden.

## Wiederholung

```C
#pragma omp for
```
Schleife parallel verarbeiten

## Beispiel

## GPU Offloading

- Wodurch unterscheidet sich eine GPU von einer CPU?
- Für welche Probleme kann man sie verwenden?

## Diskussion

- Warum und wo benötigt man message passing?

## Widerholung

- Wie ist ein "Supercomputer" aufgebaut, für welche Probleme macht er Sinn?
- Locks, Semaphoren und Monitore
