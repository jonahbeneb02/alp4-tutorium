---
theme: "metropolis"
aspectratio: 169
header-includes: \usepackage{amsmath}
---
# Tutorium 04

## Wierderholung

Ohne in die Vorlesung zu gucken überlegen:

- Was sind die Bedingungen für einen Deadlock?
- Wie kann ein bereits eingetretener Deadlock aufgelöst werden?

<!--

1. Resources are
  used exclusively.
2. Threads or processes hold allocation of a resource and
  try to allocateanother.
3. There is no preemption.With these requirements
  fulfilled the following constraint may occur and togetherwith the first
  requirements this is sufficient for a deadlock:
4. There is a cycle in the
  wait-for graph.
  
-->

## Bankers Algorithm

- $v$ - Verfügbare Resourcen
- $B$ - Belegte Resourcen
- $R$ - Angefragte Resourcen
- $f$ - Freie Resourcen

### Matrizen

- Zeilen: Threads
- Spalten: Resourcentypen

## Bankers Algorithm

\begin{align*}
  \vec{v} = (4, 4) 
\end{align*}

\begin{align*}
  B =
  \begin{pmatrix} 3 & 2 \\
  1 & 1 
  \end{pmatrix}
\end{align*}

\begin{align*}
  R = 
  \begin{pmatrix} 0 & 1 \\
  2 & 0
  \end{pmatrix}
\end{align*}

- Wie viele freie Resourcen ($f$) existieren?
- Können die angefragten Resourcen ($R$) von einem der Threads geliefert werden?
- Falls ja, wird der Thread beendet und seine Resourcen freigegeben. Können
  alle Threads beendet werden?

## Hausaufgabe

- Nachbesprechung Abgabe
- Vorstellen
- nächste Besprechen

## Diskussion

- Warum teilen sich Threads den Stack nicht?

## Gruppenarbeit

- Findet Anwendungsfälle für Semaphoren und Monitore


## Bedingungsvariablen

Mehreren anderen Threads mitteilen, dass eine Bedingung eingetreten ist, und
sie weiterlaufen können.

```
pthread_cond_wait(&cond, &mutex);
```
Wartet auf ein Signal von einem anderen Thread während ein Mutex gelockt ist.

```
pthread_cond_signal(&cond);
```
Weckt die wartenden anderen Threads.
