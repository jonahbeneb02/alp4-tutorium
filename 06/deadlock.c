#include <pthread.h>
#include <stdbool.h>

pthread_mutex_t mutex;

void* threadA (void *threadid) {
    while (true) {
        pthread_mutex_lock(&mutex);
        if (true) {
            return NULL;
        } else {
            return NULL;
        }
        pthread_mutex_unlock(&mutex);
    }
}

void* threadB (void *threadid) {
    while (true) {
        pthread_mutex_lock(&mutex);
        pthread_mutex_unlock(&mutex);
    }
}


int main() {
    pthread_t thread_a;
    pthread_t thread_b;
    pthread_create(&thread_a, NULL, threadA, (void *)0);
    pthread_create(&thread_b, NULL, threadB, (void *)1);
    
    pthread_join(thread_a, NULL);
    pthread_join(thread_b, NULL);
}
