---
theme: "metropolis"
aspectratio: 169
header-includes: |
        \usepackage{amsmath}
        \usepackage{qrcode}
---
# Tutorium 06

## Hausaufgabe

- 05 Vorstellen
- 06 Besprechen

## Debuggen mehrerer Threads mit GDB

## Anmelden auf Compute Nodes

- andorra ist nur mit SSH-Schlüssel-Authentifizierung von außerhalb der Uni
  erreichbar
- Compute Nodes sind nur über andorra erreichbar
- SSH-Schlüssel sind zwischen allen dieser Server synchronisiert

## SSH Einrichten (~/.ssh/config)

SSH-Konfiguration (Beispiel):

```Haskell
Host andorra
        HostName andorra.imp.fu-berlin.de
        User jonahbeneb02

Host compute04
        HostName compute04
        ProxyJump andorra
        User jonahbeneb02
```
## SSH Einrichten (ssh-keygen)

SSH-Key erzeugen:

```bash
ssh-keygen
```
## SSH Einrichten (ssh-copy-id)

SSH-Key auf andorra kopieren:

```bash
ssh-copy-id andorra
```
## MPI

```C
MPI_Init (&argc, &argv);
```
Initialisiert MPI, und sendet die Argumente des Programms an alle Knoten

```C
MPI_COMM_WORLD
```
Wählt alle Knoten aus

## MPI

```C
MPI_Comm_rank (communicator, &my_rank);
```
Ermittelt die ID des aktuellen Prozesses, und speichert sie in my_rank.

```C
MPI_Comm_size (communicator, &p);
```
Findet heraus, wie viele Prozesse es insgesamt gibt

## MPI

```C
MPI_Send(&message, count, datatype, dest, tag, comm);
```
Sendet eine Nachricht

```C
MPI_Recv(message, count, datatype, source, tag, comm, status);
```
Empfängt eine Nachricht. Blockiert bis sie empfangen wurde

## MPI Example

## Ambdahl's law

* Wie viel schneller kann ein Programm werden wenn es auf mehr Prozessoren läuft?
* Was beschränkt den Geschwindigkeitszuwachs?

## Ambdahl's law

$T(p) = T_s + \frac{T_p}{p}$

## Wiederholung: Bankers Algorithm

\begin{align*}
  \vec{v} = (6, 8, 3) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  4 & 4 & 1 \\
  1 & 1 & 2 \\
  0 & 3 & 0
  \end{pmatrix}
\end{align*}
\begin{align*}
  A = 
  \begin{pmatrix} 1 & 0 & 0 \\
  2 & 0 & 1 \\
  1 & 3 & 2
  \end{pmatrix}
\end{align*}

- Wie viele freie Resourcen ($f$) existieren?
- Können die angefragten Resourcen ($A$) von einem der Threads geliefert werden?
- Können alle Threads beendet werden?

## N-Body Problem

- Objekte in zwei-dimensionalem Universum (Fläche).
- Objekte haben Masse
- Sind näherungsweise punktförmig
- ziehen sich gegenseitig an
- Anziehungskraft wird durch die Masse und die Entfernung voneinander bestimmt

## Parallelisieren

- Wie könnte dieses Problem parallel simuliert werden?
