---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 9.5

## Übungszettel

- Übung 8 vorstellen
- Übung 9 besprechen

## Zeit

- fortlaufend
- unumkehrbar
- sortierbar (Zeitpunkte relativ zu anderen)

## Absolute Zeit

Ereignisse können zu einer externen Zeitquelle zugeordnet werden

Welche Quellen gibt es?

## Zeit in verteilten Systemen

Welche Probleme gibt es?

## Zeitsynchronisierung

Woher weiß man wie veraltet ein Zeitstempel von einem Zeitserver ist?

## Internet

Wie wird Zeit im Internet synchronisiert?

## Logische Uhren

- Absolute Zeit oft nicht nötig, Reihenfolge reicht
- korrekte Abhängigkeiten: Grund für ein Ereignis kann nicht nach Ereignis passiereren

## Expertengruppen

Gruppen:

- Lamport-Zeit
- Vektorzeit

Fragen:

- Generelle Funktionsweise
- Was garantiert ein Zeitstempel, und warum?

## Expertengruppen

gegenseitig Erklären

## Gegenseitiger Ausschluss in verteilten Systemen

- Warum wird die Zeit benötigt?

## Peer to Peer

- Welche Zwei großen Ansätze gibt es bei Peer to Peer Architekturen?
- Welche Probleme bringt die Suchstrategie "Flooding" mit sich?

## Peer to Peer

- Warum konnte Napster trotz Peer to Peer Architektur zentral abgeschaltet werden?
